
import { FormGroup } from '@angular/forms';
import ItemListaId from './itemListaId';
import ListaDeTrabalho from './listaDeTrabalho';
import Imovel from './imovel';
import Categoria from './categoria';
import ReconhecimentoGeografico from './reconhecimentoGeografico';

export default class ItemLista{
    id : ItemListaId;
    imovel  : Imovel;
    lista  : ListaDeTrabalho;
    reconhecimento  : ReconhecimentoGeografico;
    status : string = 'nao preenchido';
    public static popularBaseadoEmFormGroup(registro: ItemLista, formGroup: FormGroup) {
        registro.imovel =  formGroup.value.imovel;
        registro.lista =  formGroup.value.lista;
        registro.reconhecimento =  formGroup.value.reconhecimento;
       
        
    }
}
