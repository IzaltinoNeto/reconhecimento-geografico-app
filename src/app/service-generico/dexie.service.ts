import { Injectable } from '@angular/core';
import Dexie from 'dexie';
import 'dexie-observable';

@Injectable({
    providedIn: 'root'
})

export class DexieService extends Dexie {
    constructor() {
        super('Ng2Dexie');
        this.version(1).stores({
            listas: '$$id, nome',
        });
    }
}
