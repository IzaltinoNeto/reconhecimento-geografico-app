import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-de-trabalho',
  templateUrl: './lista-de-trabalho.component.html',
  styleUrls: ['./lista-de-trabalho.component.scss']
})
export class ListaDeTrabalhoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onLinkClick(event){
    console.log('event: ', event);
  }

}
