import  ListaDeTrabalho from 'src/app/model/listaDeTrabalho';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Page } from '../../model/Page';
import { HttpGenerico } from 'src/app/service-generico/http.service';
import ItemLista from 'src/app/model/itemLista';
import ItemListaId from 'src/app/model/itemListaId';
@Injectable({
    providedIn: 'root'
})
export class HttpItemListaService extends HttpGenerico<ItemLista> {

    constructor(http: HttpClient) {
        super(http, '/item-lista')
    }

   

    pesquisarPorListaId(listaId:number, sort: string, order: string, page: number, size: number): Observable<Page<ItemLista>> {
        const href = this.urlPadrao + this.urlApi ;
        
        const requestUrl =
            `${href}?listaId=${listaId}&page=${page}&size=${size}`;
            
        return this.http.get(requestUrl)
            .pipe(
                map((res: Page<ItemLista>) => res)
            );
    }

    
    pesquisarItensPorListaId(listaId:number): Observable<ItemLista[]> {
        const href = this.urlPadrao + this.urlApi;
        
        const requestUrl =
            `${href}?listaId=${listaId}`;
            
        return this.http.get(requestUrl)
            .pipe(
                map((res: ItemLista[]) => res)
            );
    }

    salvarItemLista(modelo: ItemListaId): Observable<ItemListaId> {
        console.log('modelo grupoUsuario recurso Autoridade ID',modelo);
        const href = this.urlPadrao + this.urlApi;
        const requestUrl =
            `${href}`;
        return this.http.post(requestUrl, modelo)
            .pipe(
                map((res: ItemListaId) => res)
            );
    }

    removerImovelDaLista(itemLista: ItemLista): Observable<ItemLista> {
        const href = this.urlPadrao + this.urlApi;
        const requestUrl =
            `${href}?listaId=${itemLista.id.listaId}&imovelId=${itemLista.id.imovelId}`;
        return this.http.delete(requestUrl)
            .pipe(
                map((res: ItemLista) => res)
            );
    }

    salvarItemListaInteiro(modelo: ItemLista): Observable<ItemLista> {
        console.log('modelo grupoUsuario recurso Autoridade ID',modelo);
        const href = this.urlPadrao + this.urlApi + '/salvarInteiro';
        const requestUrl =
            `${href}`;
        return this.http.post(requestUrl, modelo)
            .pipe(
                map((res: ItemLista) => res)
            );
    }

    enviarListaInteira(modelo: ListaDeTrabalho) : Observable<ListaDeTrabalho>{
        
        const href = this.urlPadrao + this.urlApi + '/atualizarListaInteira';
        
        const requestUrl =
            `${href}`;
        return this.http.put(requestUrl, modelo)
            .pipe(
                map((res: ListaDeTrabalho) => res)
            );
    }

}
