import * as Survey from "survey-angular";
import { Questao } from "../model/Questao";

export function gerarColunasHeaderTabela(questoes: Questao[], definicaoSurveyJS: any) {

    let headersTabela: any = {};
    let colunasTabela: string[] = [];
    if (questoes && questoes.length > 0) {
        questoes.map((a: any) => headersTabela[a.nome] = a.titulo)
        colunasTabela = questoes.map((a: any) => a.nome)
    } else {
        let survey = new Survey.Model(definicaoSurveyJS);
        survey.getAllQuestions().map((a: any) => headersTabela[a.name] = a.title)
        colunasTabela = survey.getAllQuestions().map((a: any) => a.name)
    }

    colunasTabela.indexOf("acoes") === -1 ? colunasTabela.push("acoes") : console.log("This item already exists");

    return { colunasTabela, headersTabela };
}

export function setarValorQuestaoSurveyJS(survey: Survey.Survey, nomeQuestao: string, valor: any) {

    let questao = survey.getQuestionByName(nomeQuestao);

    if (questao) {
        questao.value = valor;
        return true;
    }

    return false;
}